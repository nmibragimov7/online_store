const mongoose = require('mongoose');
const config = require('./app/config');
const User = require('./app/models/User');
const Category = require('./app/models/Category');
const Product = require('./app/models/Product');
const {nanoid} = require('nanoid');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('categories');
        await db.dropCollection('products');
    } catch (e) {
        console.log('Collection not found. Drop collections skiped...');
    }

    const [user1, user2] = await User.create({
        username: "admin",
        email: "admin@admin.com",
        displayName: "Admin1",
        phone: 87012223311,
        password: "admin",
        token: nanoid()
    }, {
        username: "user",
        email: "user@user.com",
        displayName: "User1",
        phone: 87015553311,
        password: "user",
        token: nanoid()
    });

    const [category1, category2, category3, category4] = await Category.create({
        category: "Computers",
        description: "description",
    }, {
        category: "Cars",
        description: "description",
    }, {
        category: "Books",
        description: "description",
    }, {
        category: "Other",
        description: "description",
    });

    await Product.create({
        user: user1._id,
        category: category1._id,
        title: "Acer",
        description: "description",
        image: "product.png",
        price: 500
    }, {
        user: user1._id,
        category: category2._id,
        title: "Toyota",
        description: "description",
        image: "product.png",
        price: 10000
    }, {
        user: user2._id,
        category: category3._id,
        title: "Rich dad, poor dad",
        description: "description",
        image: "product.png",
        price: 200
    }, {
        user: user2._id,
        category: category4._id,
        title: "Product",
        description: "description",
        image: "product.png",
        price: 50
    });

   await db.close();
});