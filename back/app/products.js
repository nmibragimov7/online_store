const express = require('express');
const router = express.Router();
const Category = require('./models/Category');
const Product = require('./models/Product');
const auth = require('./middleware/auth');
const config = require('./config');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = () =>{
    router.post('/', auth, upload.single('image'), async (req, res) => {  
        try {
            const category = await Category.find({_id: req.body.category});

            if(category.length === 0) {
                return res.status(401).send({error: "Don't select Path 'category'"});
            }
        } catch (e) {
            res.status(500).send(e);
        }
        
        const product = new Product(req.body);
        product.user = req.user._id;
        
        if(req.file){
            product.image = req.file.filename;
        }
        try{
            await product.save();
            res.send({message: "Product successfully created"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/', async (req, res) => {
        try{
            let products = [];
            if(!req.query.id) {
                products = await Product.find().populate('user').populate('category');
                return res.send(products);
            }

            products = await Product.find({category: req.query.id}).populate('user').populate('category');
            res.send(products);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/:id', async (req, res) => {
        try{
            const product = await Product.findOne({_id: req.params.id}).populate('user').populate('category');
            res.send(product);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.delete('/:id', auth, async (req, res) => {
        try {
            const product = await Product.findOne({_id: req.params.id});

            if(!product) {
                return res.status(401).send({error: "This product is not exist"});
            }

            if(req.user._id.toString() !== product.user._id.toString()) {
                return res.status(403).send({error: "Product cannot be deleted by this user"});
            }

            await Product.findByIdAndRemove({_id: req.params.id}, function (e, docs) { 
                if (e){ 
                    console.log(e);
                    return res.status(403).send({error: "Product cannot be deleted"});
                } 
            });
            res.send({message: "Product removed successfully"});
        } catch (e) {
            res.status(500).send(e);
        }
    });
    return router;
};

module.exports = createRouter;