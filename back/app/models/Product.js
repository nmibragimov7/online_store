const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        category: {
            type: Schema.Types.ObjectId,
            ref: 'Category',
            required: true
        },
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        image: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true,
            validate: {
                validator: async value => {
                    if (value < 0) return false;
                },
                message: 'The number cannot be less than 0'
            }
        }, 
    }, {versionKey: false});

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;