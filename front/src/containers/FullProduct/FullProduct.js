import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProduct, removeProduct, resetError} from "../../store/actions/productsActions";
import {loadingHandler} from "../../store/actions/loadingActions";
import Porduct from "../../components/Porduct/Porduct";
import Spinner from "../../components/Spinner/Spinner";
import {makeStyles} from "@material-ui/core/styles";
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    }
}));

const FullPorducts = (props) => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const product = useSelector(state => state.products.product);
    const errors = useSelector(state => state.products.errors);
    const loading = useSelector(state => state.loading.loading);

    useEffect(()=> {
        dispatch(resetError());
        dispatch(loadingHandler());
        dispatch(fetchProduct(props.match.params.id));
    }, [dispatch]);

    const removeProductHandler = () => {
        dispatch(removeProduct(props.match.params.id));
    };

    let form = (
        <>
            {errors && errors.error &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            <Porduct
            key={product._id}
            title={product.title}
            description={product.description}
            price={product.price}
            image={product.image}
            userName={product.user}
            category={product.category}
            user={user}
            fullProduct={true}
            removeProduct={removeProductHandler}
            />  

        </>
    );

    if (loading) {
        form = <Spinner />;
    }

    return (
        <>
            {form}
        </>
    );
};

export default FullPorducts;