import React, {useEffect} from 'react';
import AddProductForm from "../../components/AddProductForm/AddProductForm";
import {useSelector, useDispatch} from "react-redux";
import {fetchCategories, createProduct, resetError} from "../../store/actions/productsActions";

const AddProduct = props => {
    const dispatch = useDispatch();
    const categories = useSelector(state => state.products.categories);

    useEffect(()=> {
        dispatch(fetchCategories());
        dispatch(resetError());
    }, [dispatch]);

    const onProductFormSubmit = product => {
        dispatch(createProduct(product));
    };

    return (
        <>
            <AddProductForm 
            categories={categories}
            onSubmit={onProductFormSubmit}
            />
        </>
    );
};

export default AddProduct;