import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {apiURL} from "../../config";
import CardProduct from '../UI/CardProduct/CardProduct';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        alignItems: "center",
        border: "1px solid",
        borderColor: "rgb(187, 195, 204)",
        padding: "10px",
        marginBottom: "20px"
    },
    image: {
        display: "block",
        minWidth: "150px",
        maxWidth: "250px",
        height: "auto",
        marginRight: "30px",    
    }
}));

const Product = (props) => {
    const classes = useStyles();
    const [imageClasses, setImageClasses] = useState({});

    let productImage = "";
    if(props.image) {
        productImage = apiURL + "/uploads/" + props.image;
    }

    let userName = "";
    if(props.userName) {
        userName = props.userName.displayName;
    }

    let category = "";
    if(props.userName) {
        category = props.category.category;
    }

    const errorHandler = () => {
        setImageClasses({visibility: "hidden"});
    };
    
    return (
        <>
            {props.fullProduct ? 
            (
                <>
                    <div className={classes.root}>
                        <img src={productImage} alt={props.title} className={classes.image} style={imageClasses} onError={errorHandler}/>
                        <div>
                            <h1>{props.title}</h1>
                            <p>Category: {category}</p>
                            <p>Seller: {userName}</p>
                            <p>Description: {props.description}</p>
                            <p>Price: {props.price} $</p>
                        </div>
                    </div>
                    {props.user ? (
                    <Button 
                    variant="contained" 
                    color="secondary"
                    onClick={props.removeProduct}
                    >
                        Delete
                    </Button>
                ) : null}
                </>
            ) : (
                <>
                    <CardProduct
                    title={props.title}
                    image={productImage}
                    price={props.price}
                    blockHandler={props.blockHandler}
                    />
                </>
            )}
        </>
        
    );
};

export default Product;