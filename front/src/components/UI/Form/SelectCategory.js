import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

 const SelectCategory = (props) => {
    const classes = useStyles();

    return (
        <FormControl required className={classes.formControl}>
            <InputLabel id="category-native-required">Category</InputLabel>
            <Select
            native
            value={props.inputs.category}
            onChange={props.inputChangeHandler}
            name="category"
            inputProps={{
                id: 'category-native-required',
            }}
            >
                <option aria-label="None" value={null} />
                {props.categories.map(category => (
                    <option key={category._id} value={category._id}>{category.category}</option>
                ))}
            </Select>
            <FormHelperText>Required</FormHelperText>
        </FormControl>
    );
}

export default SelectCategory;