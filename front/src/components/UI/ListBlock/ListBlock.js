import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 200,
        backgroundColor: theme.palette.background.paper,
        textDecoration: "underline",
    },
    category: {
        '&:hover': {
            color: "rgb(187, 195, 204)",
            textDecoration: "none",
        }
    }
}));

const ListBlock = (props) => {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      <ListItem>
        <ListItemText 
        primary="All products" 
        onClick={() => props.fetchProductsByCategory(null)}
        className={classes.category}
        />
      </ListItem>
      {props.categories.map(category => (
            <ListItem key={category._id}>
                <ListItemText 
                primary={category.category}
                onClick={() => props.fetchProductsByCategory(category._id)}
                className={classes.category}
                />
            </ListItem>
        ))}
    </List>
  );
}

export default ListBlock;