import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {Link, NavLink} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import {logoutUser} from "../../store/actions/usersAction";
import {useDispatch} from "react-redux";

const useStyles = makeStyles(theme => ({
    mainLink: {
        color: 'inherit',
        textDecoration: 'none',
        '&:hover': {
            opacity: "0.5"
        }
    },
    staticToolbar: {
        marginBottom: theme.spacing(2)
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1
    },
    menu: {
        marginTop: "30px"
    }
}));
const AppToolbar = ({user}, props) => {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const logout = () => {
        dispatch(logoutUser());
    };

    return (
        <>
          <AppBar position="fixed">
            <Toolbar className={classes.toolbar}>
                <Typography variant="h6" className={classes.toolbarTitle}>
                    <Link to="/" className={classes.mainLink}>Market</Link>
                </Typography>
                {user ? (
                    <>
                    <Button
                        color="inherit"
                        aria-controls="simple-menu"
                        aria-haspopup="true"
                        onClick={handleClick}
                    >
                        Hello, {user.displayName}
                    </Button>
                    <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    className={classes.menu}
                    >
                        <MenuItem onClick={handleClose}>
                            <Link to="/new_product" className={classes.mainLink}>
                                Add new product
                            </Link>
                        </MenuItem>
                        <MenuItem onClick={logout}>Logout</MenuItem>
                    </Menu>
                    </>
                    ) : (
                        <>
                            <Button to="/login" className={classes.link} color="inherit" variant="outlined" component={NavLink}>Sign In</Button>
                            <Button to="/register" className={classes.link} color="inherit" variant="outlined" component={NavLink}>Sign Up</Button>
                        </>
                )}
            </Toolbar>
          </AppBar>
            <Toolbar className={classes.staticToolbar}/>
        </>
    );
};

export default AppToolbar;