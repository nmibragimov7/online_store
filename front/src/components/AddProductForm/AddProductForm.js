import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import FileInput from "../UI/Form/FileInput";
import {useSelector} from "react-redux";
import TextInput from "../UI/Form/TextInput";
import SelectCategory from '../UI/Form/SelectCategory';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    }
}));

const AddProductForm = ({onSubmit, categories}) => {
    const classes = useStyles();
    const [state, setState] = useState({
        title: "",
        description: "",
        price: "",
        image: "",
        category: ""
    });

    const errors = useSelector(state => state.products.errors);

    const inputChangeHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        })
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState =>({
            ...prevState,
            [name]: file
            })
        )
    };
    
    const submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key])
        });
        onSubmit(formData);
    };

    const getFieldError = fieldName => {
        try {
            return errors.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
        <Grid container direction="column" spacing={2}>
            <TextInput
                label="title"
                onChange={inputChangeHandler}
                name="title"
                error={getFieldError('title')}
                required={true}
            />
            <TextInput
                label="description"
                onChange={inputChangeHandler}
                name="description"
                error={getFieldError('description')}
                required={true}
            />
            <TextInput
                label="price"
                onChange={inputChangeHandler}
                name="price"
                error={getFieldError('price')}
                required={true}
            />
            <Grid item>
                <FileInput
                    name="image"
                    label="Image"
                    onChange={fileChangeHandler}
                    error={getFieldError('description')}
                />
            </Grid> 
            <Grid item>
                <SelectCategory
                    inputs={state}
                    categories={categories}
                    inputChangeHandler={inputChangeHandler}
                />
            </Grid> 
            <Grid item>
                <Button type="submit" color="primary" variant="contained">Create product</Button>
            </Grid>
        </Grid>
        </form>
    );
};

export default AddProductForm;