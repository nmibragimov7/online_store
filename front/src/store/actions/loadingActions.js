export const LOADING_HANDLER = 'LOADING_HANDLER';

export const loadingHandler = () => ({type: LOADING_HANDLER, loading: true});
export const loadingOffHandler = () => ({type: LOADING_HANDLER, loading: false});
