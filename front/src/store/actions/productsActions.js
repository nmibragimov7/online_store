import axiosApi from "../../axiosApi";
import {loadingOffHandler} from "./loadingActions";
import {push} from "connected-react-router";

export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAILURE = 'CREATE_PRODUCT_FAILURE';
export const REMOVE_PRODUCT_SUCCESS = 'REMOVE_PRODUCT_SUCCESS';
export const REMOVE_PRODUCT_FAILURE = 'REMOVE_PRODUCT_FAILURE';
export const RESET_ERROR = 'RESET_ERROR';
export const RESET_MESSAGE = 'RESET_MESSAGE';
 
const fetchCategoriesSuccess = categories => {
    return {type: FETCH_CATEGORIES_SUCCESS, categories}
};

const fetchProductsSuccess = products => {
    return {type: FETCH_PRODUCTS_SUCCESS, products}
};

const fetchProductSuccess = product => {
    return {type: FETCH_PRODUCT_SUCCESS, product}
};

export const resetError = () => {
    return {type: RESET_ERROR}
};

export const resetMessage = () => {
    return {type: RESET_MESSAGE}
};

const createProductSuccess = (message) => {
    return {type: CREATE_PRODUCT_SUCCESS, message}
};

const createProductFailure = (errors) => {
    return {type: CREATE_PRODUCT_FAILURE, errors}
};

const removeProductSuccess = (message) => {
    return {type: REMOVE_PRODUCT_SUCCESS, message}
};

const removeProductFailure = (errors) => {
    return {type: REMOVE_PRODUCT_FAILURE, errors}
};

export const fetchCategories = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/categories');
            dispatch(fetchCategoriesSuccess(response.data));
        } catch(e) {
            console.error(e);
        }
    }
};

export const fetchProducts = (category) => {
    const categoryId = {id: category};

    return async dispatch => {
        try {
            const response = await axiosApi.get('/products', {params: categoryId});
            dispatch(fetchProductsSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const fetchProduct = (id) => {
    return async dispatch => {
        try {
            const response = await axiosApi.get(`/products/${id}`);
            dispatch(fetchProductSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const createProduct = (product) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axiosApi.post(`/products`, product, {headers});
            dispatch(createProductSuccess(response.data));
            dispatch(push('/'));
        } catch(e) {
            dispatch(createProductFailure(e.response.data));
            console.error(e);
        }
    }
};

export const removeProduct = (id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};
            const response = await axiosApi.delete(`/products/${id}`, {headers});
            dispatch(removeProductSuccess(response.data));
            dispatch(push('/'));
        } catch(e) {
            dispatch(removeProductFailure(e.response.data));
            console.error(e);
        }
    }
};