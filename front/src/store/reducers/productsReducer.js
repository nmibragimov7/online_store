import {
    FETCH_CATEGORIES_SUCCESS, 
    FETCH_PRODUCTS_SUCCESS, 
    FETCH_PRODUCT_SUCCESS, 
    CREATE_PRODUCT_SUCCESS,
    CREATE_PRODUCT_FAILURE,
    REMOVE_PRODUCT_SUCCESS,
    REMOVE_PRODUCT_FAILURE,
    RESET_ERROR,
    RESET_MESSAGE
} from "../actions/productsActions";

const initialState = {
    categories: [],
    products: [],
    product: {},
    message: null,
    errors: null
};

const postsReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.categories}
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products}
        case FETCH_PRODUCT_SUCCESS:
            return {...state, product: action.product}
        case CREATE_PRODUCT_SUCCESS:
            return {...state, errors: null, message: action.message}
        case CREATE_PRODUCT_FAILURE:
            return {...state, errors: action.errors}
        case REMOVE_PRODUCT_SUCCESS:
            return {...state, errors: null, message: action.message}
        case REMOVE_PRODUCT_FAILURE:
            return {...state, errors: action.errors}
        case RESET_ERROR:
            return {...state, errors: null}
        case RESET_MESSAGE:
            return {...state, message: null}
        default:
            return state;
    }
};

export default postsReducer;