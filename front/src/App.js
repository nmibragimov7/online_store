import React from 'react';
import Container from "@material-ui/core/Container";
import {Route, Switch} from "react-router-dom"
import Products from "./containers/Products/Products";
import AddProduct from "./containers/AddProduct/AddProduct";
import FullProduct from "./containers/FullProduct/FullProduct";
import AppToolbar from "./components/AppToolbar/AppToolbar";
import Register from "./containers/User/Register";
import Login from "./containers/User/Login";
import {useSelector} from "react-redux";

const App = (props) => {
  const user = useSelector(store => store.users.user);

  return(
    <>
      <header><AppToolbar user={user}/></header>
      <main>
        <Container>
          <Switch>
            <Route path="/" exact component={Products}/>
            <Route path="/new_product" exact component={AddProduct}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/:id" component={FullProduct}/>
            <Route path='/' render={()=>(<div><h1>404 Not found</h1></div>)}/>
          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;
